#Cellular automaton model of plant population growth #

This repository holds code for the cellular automata presented in the BSC6932 Mathematical Biology lectures. The implementation of the plant population growth CA is based on the models presented in [Kalmykov & Kalmykov 2014](http://dx.doi.org/10.7287/peerj.preprints.762v1).

N.B. The code is fragile, in particular the scaling of the lattice plots has to be adjusted manually at the moment.